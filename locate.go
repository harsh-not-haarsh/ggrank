package main

import (
	"net/http"
	"regexp"

	"github.com/PuerkitoBio/goquery"
)

func isWhiteListed(url string, whitelisted []string) bool {

	// Get the HTML
	resp, err := http.Get(url)
	if err != nil {
		// log.Println("Publisher not found\n", err)
		return true
	}

	// Convert HTML into goquery document
	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		// log.Println(err)
		return true
	}
	resp.Body.Close()

	present := false
	for _, country := range whitelisted {
		r := regexp.MustCompile("Location: " + country)
		doc.Find("span.XjE2Pb").Each(func(i int, s *goquery.Selection) {
			if r.MatchString(s.Text()) {
				present = true
			}
		})
	}

	return present
}

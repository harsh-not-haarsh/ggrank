package main

import (
	"math"
	"strconv"
	"strings"
	"sync"
	"time"
)

const activityLimit = 2500000

// Publisher contains apps published by a publisher along with ggRank
type Publisher struct {
	ID      string  `json:"publisherID"`
	Apps    []App   `json:"apps"`
	GGScore float64 `json:"ggScore"`
	GGRank  int     `json:"ggRank"`
}

func (pub *Publisher) getGGScore() {
	appActivity := 0.0
	for _, app := range pub.Apps {
		year := (time.Hour * 24 * 365).Hours()
		activityConstant := 1 + (year / (year + time.Now().Sub(app.lastUpdated).Hours()))
		if app.ratings > 1 {
			appActivity += activityConstant * math.Log2(app.ratings) * float64(app.installs)
		}
	}
	if appActivity < activityLimit && pub.isWhiteListed([]string{"India"}) {
		pub.GGScore = math.Log10(appActivity)
	} else {
		pub.GGScore = 0
	}
}

func (pub *Publisher) isWhiteListed(whitelisted []string) bool {
	var publisher string

	if containsLetter(pub.ID) {
		publisher = "https://play.google.com/store/apps/developer?id=" + pub.ID
	} else {
		publisher = "https://play.google.com/store/apps/dev?id=" + pub.ID
	}

	return isWhiteListed(publisher, whitelisted)
}

func getPublishers(lines [][]string) (map[string]Publisher, chan bool) {
	store := make(map[string]Publisher)
	done := make(chan bool)
	temp := make(chan bool)
	var mutex = &sync.Mutex{}
	apps := make(chan App, 10000)

	go func() {
		for i, line := range lines {
			println("==>", i)
			rating, _ := strconv.ParseFloat(line[1], 64)
			installs, _ := strconv.ParseInt(strings.Join(strings.Split(strings.Split(line[5], " ")[0], ","), ""), 10, 64)
			lastUpdated, _ := time.Parse("January 2, 2006", line[9])

			app := App{
				Name:        line[0],
				ratings:     rating,
				publisherID: line[4],
				installs:    installs,
				Genre:       line[6],
				Email:       line[8],
				lastUpdated: lastUpdated,
				URL:         line[7],
			}
			apps <- app
		}
		temp <- true
	}()
	i := 0
	go func() {
		for j := 0; j < 100; j++ {
			go func() {
				for app := range apps {
					i++
					mutex.Lock()
					publisher, ok := store[app.publisherID]
					mutex.Unlock()
					if ok {
						publisher.Apps = append(publisher.Apps, app)
						mutex.Lock()
						store[publisher.ID] = publisher
						mutex.Unlock()

					} else {
						pub := Publisher{
							ID:      app.publisherID,
							Apps:    []App{app},
							GGScore: 0.0,
							GGRank:  0,
						}
						if pub.isWhiteListed([]string{"India"}) {
							mutex.Lock()
							store[app.publisherID] = pub
							mutex.Unlock()
						}
					}
					println(i)
				}
			}()
		}
	}()
	<-temp
	close(apps)
	done <- true
	return store, done
}

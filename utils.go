package main

import (
	"encoding/csv"
	"os"
)

func readCSV(filename string) ([][]string, error) {
	f, err := os.Open(filename)
	if err != nil {
		return [][]string{}, err
	}
	defer f.Close()

	lines, err := csv.NewReader(f).ReadAll()
	if err != nil {
		return [][]string{}, err
	}
	return lines, nil
}

func containsLetter(s string) bool {
	for _, x := range s {
		if x > '9' || x < '0' {
			return true
		}
	}
	return false
}

func toSlice(publishers chan Publisher) []Publisher {
	var retPub []Publisher
	for pub := range publishers {
		retPub = append(retPub, pub)
	}
	println(len(retPub))
	return retPub
}

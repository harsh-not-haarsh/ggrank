package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/chromedp/chromedp"
)

// App contains information available for an app
type App struct {
	Name        string   `json:"name"`
	Genre       string   `json:"Genre"`
	Email       string   `json:"Email"`
	URL         string   `json:"url"`
	Comments    []string `json:"comments"`
	ratings     float64
	publisherID string
	installs    int64
	lastUpdated time.Time
}

func (app *App) getComments() {
	opts := append(chromedp.DefaultExecAllocatorOptions[:],
		chromedp.Flag("headless", true),
		chromedp.Flag("disable-gpu", false),
		chromedp.Flag("enable-automation", false),
		chromedp.Flag("disable-extensions", true),
	)

	allocCtx, cancel := chromedp.NewExecAllocator(context.Background(), opts...)
	defer cancel()

	ctx, cancel := chromedp.NewContext(allocCtx, chromedp.WithLogf(log.Printf))
	defer cancel()

	var out []string
	var n int
	var res []byte
	if err := chromedp.Run(ctx,
		chromedp.Navigate(app.URL),
		chromedp.Evaluate(`window.scrollTo(0,document.body.scrollHeight);`, &res),
		chromedp.Sleep(2500*time.Millisecond),
		chromedp.Evaluate(`window.scrollTo(0,document.body.scrollHeight);`, &res),
		chromedp.Sleep(2500*time.Millisecond),
		chromedp.Evaluate(`window.scrollTo(0,document.body.scrollHeight);`, &res),
		chromedp.Sleep(2500*time.Millisecond),
		chromedp.Evaluate(`window.scrollTo(0,document.body.scrollHeight);`, &res),
		chromedp.Sleep(2000*time.Millisecond),
		chromedp.Evaluate(`window.scrollTo(0,document.body.scrollHeight);`, &res),
		chromedp.Sleep(1500*time.Millisecond),
		chromedp.Evaluate(`window.scrollTo(0,document.body.scrollHeight);`, &res),
		chromedp.EvaluateAsDevTools(`document.getElementsByClassName("UD7Dzf").length;`, &n),
		chromedp.EvaluateAsDevTools(`Array.from(document.getElementsByClassName("UD7Dzf")).map(a => a.getElementsByTagName('span')[0]).map(a => a.innerText);`, &out),
	); err != nil {
		log.Fatal(err)
	}
	fmt.Println("====>", n)
	app.Comments = out
	chromedp.Cancel(ctx)

}

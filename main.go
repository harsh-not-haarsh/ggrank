package main

import (
	"fmt"
)

func main() {
	apps, err := readCSV("out.csv")
	if err != nil {
		panic(err)
	}
	println("read data")

	var (
		done = make(chan bool)

		// totalPublishers = len(publisherMap)

		count = 0

		// rankedPublishers []Publisher

		// allPublishers   = make(chan Publisher, 100)
		// scoredPublisher = make(chan Publisher, 1000)

		// wg sync.WaitGroup
	)
	publisherMap, done := getPublishers(apps) //publisherMap is map[<string> publisherID]Publisher{}
	<-done
	fmt.Println((publisherMap))
	for _, pub := range publisherMap {
		count += len(pub.Apps)
	}
	println(count)

	// go func() {
	// 	for _, publisher := range publisherMap {
	// 		allPublishers <- publisher
	// 	}
	// 	close(allPublishers)
	// 	done <- true
	// }()
	// fmt.Println("Reading Data...")

	// go func() {
	// 	for i := 0; i < 30; i++ {
	// 		for pub := range allPublishers {
	// 			wg.Add(1)
	// 			go func(pub Publisher, wg *sync.WaitGroup) {
	// 				pub.getGGScore()
	// 				scoredPublisher <- pub
	// 				wg.Done()
	// 			}(pub, &wg)
	// 		}
	// 	}
	// }()

	// go func() {
	// 	rankedPublishers = getGGRank(toSlice(scoredPublisher)) // Publishers ranked with GGScore
	// }()

	// <-done
	// fmt.Println("Calculating GG Score...")

	// wg.Wait()
	// close(scoredPublisher)

	// for i, publisher := range rankedPublishers {
	// 	for j, app := range publisher.Apps {
	// 		app.getComments()
	// 		publisher.Apps[j] = app
	// 	}
	// 	rankedPublishers[i] = publisher
	// }

	// info, err := json.Marshal(rankedPublishers)
	// if err != nil {
	// 	log.Fatal("Cannot encode to JSON ", err)
	// }
	// fmt.Println(info)
}

package main

import (
	"fmt"
	"sort"
)

func getGGRank(allPublishers []Publisher) []Publisher {
	fmt.Println("Calculating GG Rank...")

	var rankedPub []Publisher

	sort.Slice(allPublishers[:], func(i, j int) bool {
		return allPublishers[i].GGScore > allPublishers[j].GGScore
	})

	for i := range allPublishers {
		pub := allPublishers[i]
		pub.GGRank = i + 1
		rankedPub = append(rankedPub, pub)

		if allPublishers[i].GGScore < 1 {
			break
		}
	}

	return rankedPub
}
